/**
 * Bài 1: Tìm số nguyên dương nhỏ nhất
 */
function ketQua1() {
  var sum = 0;
  var i = 0;
  while (sum <= 10000) {
    sum += ++i;
  }
  return (document.getElementById(
    "inketqua1"
  ).innerText = ` Số tự nhiên nhỏ nhất thoã điều kiện là: ${i}`);
}

/**
 * Bài 2: Tính tổng
 */
function ketQua2() {
  var x = document.getElementById("x").value * 1;
  var n = document.getElementById("n").value * 1;
  var muCuaX = 1;
  for (var Sn = 0; muCuaX <= n && n >= 1; muCuaX++) {
    Sn = Sn + x ** muCuaX;
  }
  return (document.getElementById(
    "inketqua2"
  ).innerText = ` Kết quả là: ${Sn}`);
}

/**
 * Bài 3: Tính giai thừa
 */
function ketQua3() {
  var num3 = document.getElementById("num3").value * 1;
  var gaiThua = 1;

  if (num3 == 0) {
    gaiThua = gaiThua;
  } else if (num3 < 0) {
    gaiThua = "Số nhập không hợp lệ";
  } else {
    for (var i = 1; num3 > 0 && i <= num3; i++) {
      gaiThua *= i;
    }
  }
  return (document.getElementById(
    "inketqua3"
  ).innerText = ` Kết quả : ${gaiThua}`);
}

/**
 * Bài 4: Tạo thẻ div
 */
function ketQua4() {
  var content = "";
  for (var i = 1; i <= 10; i++) {
    if (i % 2 == 0) {
      content = `${content} <div class="bg-danger text-white py-1">Div chẵn</div>`;
    } else {
      content = `${content} <div class="bg-primary text-white py-1">Div lẻ</div>`;
    }
  }
  return (document.getElementById("inketqua4").innerHTML = content);
}
